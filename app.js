"use strict";

// INITIALIZATION
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var https = require('https');
var request = require('request');
var hbs = require('hbs');
hbs.registerPartials('public');

app.set('view engine', 'hbs');
app.set('views', 'public');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use(express.static('public'));

// MONGODB & MONGOOSE SETUP
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://admin:password@ds235169.mlab.com:35169/db_jokeservice");

// ROUTES FOR THE APP
var jokeRouter = require('./routes/jokes')(express);
var idRouter = require('./routes/id')(express);
var rootRouter = require('./routes/root')(express);
app.use(jokeRouter);
app.use(idRouter);
app.use(rootRouter);

// START THE SERVER
var port = process.env.PORT || 8080;
app.listen(port)

console.log('Listening on port ' + port + ' ...');

request.post({
    url:     'https://krdo-joke-registry.herokuapp.com/api/services',
    form:    {
        "name": "Epic memers",
        "address": "https://testerbois.herokuapp.com/",
        "secret": "hejsa"
    }
}, function(error, response, body){
    console.log(body);
});