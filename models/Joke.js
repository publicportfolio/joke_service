var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var joke = new Schema({
    setup: String,
    punchline: String,
});

module.exports = mongoose.model('Joke', joke);