var Joke = require('../models/Joke');

module.exports = function(express){
    var router = express.Router();

    router.route('/api/jokes/:id')
        .get(function(req,res){

            var id = req.params.id;

            Joke.findById(id,function(err,data){

                if(err){
                    res.send("error")
                }
                else{
                    res.send(data);
                }
            })
    }).delete(function(req,res){

        var id = req.params.id;

        Joke.findByIdAndRemove(id,function(err){
            if(err){
                res.send(err)
            }
            else{
                res.send("SUCCESS")
            }
        })
    }).put(function(req,res){

        var newPunchline = req.body.updatedPunchline;
        var newSetupline = req.body.updatedSetup;

        var id = req.params.id;

        Joke.findByIdAndUpdate(id,{setup:newSetupline,punchline:newPunchline},function(err){
            if(err){
                res.send(err)
            }
            else{
                res.send("UPDATED")
            }

        })


    })
return router;
}