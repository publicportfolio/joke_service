var Joke = require('../models/Joke');

module.exports = function(express) {
    var router = express.Router();
    router.route('/api/jokes')
        .get(function (req, res) {
            Joke.find(function(err, dbRes){
                if(err){
                    res.send();
                }else{
                    res.send(dbRes);
                }
            });
        })
        .post(function (req, res) {
            var joke = new Joke({
                setup : req.body.setup,
                punchline : req.body.punchline
            });

            joke.save(function(err, dbMes){
                if(err){
                    res.send();
                }else{
                    res.send("Saved");
                }
            });
        });
    return router;
};