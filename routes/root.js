var Joke = require('../models/Joke');
var rq = require('request-promise');

var options = {
    uri: 'https://krdo-joke-registry.herokuapp.com/api/services',
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true
};

module.exports = function (express) {
    var router = express.Router();
    router.route('/')
        .get(function (req, res) {
            var dbResult = null;

            Joke.find(function (err, dbRes) {
                if (err) {
                    res.send(err);
                }
                else {
                    dbResult = dbRes;
                }
            });

            rq(options).then(function (results) {
                var dealers = [];

                for (var i = 0; i < results.length; i++) {
                    if (results[i].address == "https://testerbois.herokuapp.com/" || results[i].address == null) {
                    } else {
                        if (results[i].address.charAt(results[i].address.length - 1) == '/') {
                            dealers.push(new JokeDealer(results[i].timestamp, results[i].name, results[i].address + "api/jokes"));
                        } else {
                            dealers.push(new JokeDealer(results[i].timestamp, results[i].name, results[i].address + "/api/jokes"));
                        }
                    }
                }
                return dealers;
            }).then(function (inputDealers) {

                var promises = [];
                var result = [];

                //Ikke lavet: skal håndtere når der opstår en 404
                for (var i = 0; i < inputDealers.length; i++) {
                    promises.push(rq(inputDealers[i].address).catch(function (err) {
                    }));
                }

                Promise.all(promises).then(function (resultat) {
                    for (var i = 0; i < inputDealers.length; i++) {
                        if ((resultat[i] != null || resultat[i] != undefined) && resultat[i].charAt(0) != '<') {
                            var data = JSON.parse(resultat[i]);
                            result.push(inputDealers[i]);
                            for (var j = 0; j < data.length; j++) {
                                result.push(data[j]);
                            }
                        }
                    }
                    return result;
                }).then(function (resultat) {

                    res.render('index', {
                        'jokes': dbResult,
                        'dealersAndJokes': resultat
                    })
                })

            }).catch(function (err) {
                res.send(err)
            })


        }).post(function (req, res) {
        var joke = new Joke({
            setup: req.body.setup,
            punchline: req.body.punchline
        });

        joke.save(function (err, dbMes) {
            if (err) {
                res.send();
            } else {
                res.send();
            }
        });
    });
    return router;
};


function JokeDealer(timestamp, name, address) {
    this.timestamp = timestamp;
    this.name = name;
    this.address = address;
}