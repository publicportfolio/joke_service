$(function(){

// CREATE //
    $("#addJokeButton").click(function(){
        $('#create-message').empty()
        $('#addJokeSetup').removeClass("error");
        $('#addJokePunchline').removeClass("error");

        if($('#addJokePunchline').val() == "" || $('#addJokeSetup').val() == ""){

            if($('#addJokeSetup').val() == ""){
                $('#addJokeSetup').addClass("error");
            }
            if($('#addJokePunchline').val() == ""){
                $('#addJokePunchline').addClass("error");
            }
        }
        else{

            var data = {
                punchline: $('#addJokePunchline').val(),
                setup:$('#addJokeSetup').val()
            };

            $.post("/api/jokes",data)
                .then(function(data){

                    $('#create-message').parent().addClass("alert").addClass("alert-success");
                    $('#create-message').append("Joken er gemt")
                    setTimeout(function(){ location.reload(); }, 2000)

                }).catch(function(err){
                console.log(err)
            })
        }
    });

    // DELETE //
    $("#removeJokeButton").click(function(){

        var succesDelete = false;
        var jokesToDelete = $(".deleteCheckbox:checkbox:checked")
        $("#delete-message").empty();
        $('#delete-message').parent().removeClass("alert").removeClass("alert-danger");

        if(jokesToDelete.length === 0)
        {
            $('#delete-message').parent().addClass("alert").addClass("alert-danger");
            $("#delete-message").append("Der er ingen jokes valgt!")
        }
        else{
            for(var i = 0; i<jokesToDelete.length;i++)
            {
                $.ajax({
                    url: '/api/jokes/' + jokesToDelete[i].id,
                    type: 'DELETE',
                    success: function(data) {
                        if(!succesDelete){
                            $('#delete-message').parent().addClass("alert").addClass("alert-success");
                            $('#delete-message').append("Elementerne er slettet")
                            succesDelete = true;
                            setTimeout(function(){ location.reload(); }, 2000)
                        }
                    }
                });
            }
        }
    });

// UPDATE //
    $(".editButton").click(function(){
        var id = $(this).attr('id');
        $("#updateJokeButton").data('toUpdate', id);
        $.ajax({
            url: '/api/jokes/' + id,
            type: 'GET',
            success: function(data) {
                $("#updateJokeSetup").val(data.setup);
                $("#updateJokePunchline").val(data.punchline);
            }
        });
    })

    $("#updateJokeButton").click(function(){

        $('#update-message').empty()
        $('#updateJokeSetup').removeClass("error");
        $('#updateJokePunchline').removeClass("error");

        if($('#updateJokePunchline').val() == "" || $('#updateJokeSetup').val() == ""){

            if($('#updateJokeSetup').val() == ""){
                $('#updateJokeSetup').addClass("error");
            }

            if($('#updateJokePunchline').val() == ""){
                $('#updateJokePunchline').addClass("error");
            }

        }
        else{

            var id = $("#updateJokeButton").data('toUpdate');

            var data = {
                updatedPunchline: $('#updateJokePunchline').val(),
                updatedSetup:$('#updateJokeSetup').val()
            };

            var jokeToUpdate = id;

            $.ajax({
                url: '/api/jokes/' + jokeToUpdate,
                type: 'PUT',
                data:data,
                success: function(data) {
                    $('#update-message').parent().addClass("alert").addClass("alert-success");
                    $('#update-message').append("Joken er blevet opdateret")
                    setTimeout(function(){ location.reload(); }, 500)
                }
            });
        }
    });


})






